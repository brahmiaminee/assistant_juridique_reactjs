import React, { createContext, useEffect, useReducer } from "react";
let reducer = (token, newToken) => {
  if (newToken == "") {
    localStorage.removeItem("token");
    return "";
  }
  return newToken;
};

/* It's getting the form from localStorage. */
const formLocalState = localStorage.getItem("token");
export const GlobalContext = createContext();

function Global({ children }) {
  const [token, setToken] = useReducer(reducer, formLocalState || "");

  /* It's setting the form in localStorage. */
  useEffect(() => {
    localStorage.setItem("token", token);
  }, [token]);

  return (
    <GlobalContext.Provider
      value={{
        token,
        setToken,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
}

export default Global;
