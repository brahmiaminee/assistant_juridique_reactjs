import React from "react";
import Header from "../components/shared/Header";
import Footer from "../components/shared/Footer";

//document.body.classList.remove("pg-auth");

function EmptyLayout(props) {
  return (
    <>
      <Header></Header>
      <React.Fragment>{props.children}</React.Fragment>
      <Footer></Footer>
    </>
  );
}

export default EmptyLayout;
