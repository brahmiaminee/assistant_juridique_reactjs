import React from "react";
import Footer from "../components/shared/Footer";
import Header from "../components/shared/Header";

function DashboardLayout(props) {
  return (
    <>
      <Header></Header>
      <section className="question-area pt-80px pb-30px">
        <div className="container">
          <React.Fragment>{props.children}</React.Fragment>
        </div>
      </section>
      <Footer></Footer>
    </>
  );
}

export default DashboardLayout;
