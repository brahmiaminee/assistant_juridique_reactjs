import "./App.css";
import React from "react";
//import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Home from "./components/Home";
import DashboardLayout from "./layouts/DashboardLayout";
import LoginPage from "./pages/LoginPage";
import EmptyLayout from "./layouts/EmptyLayout";
import RegisterPage from "./pages/RegisterPage";
import ForgotPassword from "./pages/ForgotPassword";
import Contact from "./pages/Contact";
import Header from "./components/shared/Header";
import Footer from "./components/shared/Footer";
import Profil from "./components/user/Profil";
import ListHistory from "./components/histories/ListHistory";
import ListQuestion from "./components/histories/ListQuestion";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          exact
          element={
            <DashboardLayout>
              <Home />
            </DashboardLayout>
          }
        />
        <Route
          path="/login"
          exact
          element={
            <EmptyLayout>
              <LoginPage />
            </EmptyLayout>
          }
        />

        <Route
          path="/register"
          exact
          element={
            <EmptyLayout>
              <RegisterPage />
            </EmptyLayout>
          }
        />

        <Route
          path="/forget_password"
          exact
          element={
            <EmptyLayout>
              <ForgotPassword />
            </EmptyLayout>
          }
        />

        <Route
          path="/contact"
          exact
          element={
            <EmptyLayout>
              <Contact />
            </EmptyLayout>
          }
        />
        <Route
          path="/history"
          exact
          element={
            <EmptyLayout>
              <ListQuestion />
            </EmptyLayout>
          }
        />
        <Route
          path="/profil"
          exact
          element={
            <DashboardLayout>
              <Profil />
            </DashboardLayout>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
