import React, { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useToken } from "../auth/useToken";
import { login } from "../services/userApi";
import { GlobalContext } from "../context/Global";

function LoginPage() {
  const navigate = useNavigate();
  const { token, setToken } = useContext(GlobalContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = (e) => {
    e.preventDefault();
    if (email != null || password != null) {
      login(email, password)
        .then((res) => {
          const token = res.data.token;
          console.log(token);
          setToken(token);
          setTimeout(() => {
            //window.location.reload(false);
            navigate("/");
          }, 100);
        })
        .catch(() => {
          alert("email ou mot de passe invalide");
        });
    } else {
      alert("veuillez remplir tous les champs");
    }
  };
  return (
    <section className="sign-up-area pt-80px pb-80px position-relative">
      <div className="container">
        <form action="#" className="card card-item" onSubmit={(e) => handleLogin(e)}>
          <div className="card-body row p-0">
            <div className="col-lg-6">
              <div className="form-content py-4 pr-60px pl-60px border-right border-right-gray h-100 d-flex align-items-center justify-content-center">
                <img src="images/undraw-remotely.svg" alt="Image" className="img-fluid" />
              </div>
            </div>
            {/* end col-lg-6 */}
            <div className="col-lg-5 mx-auto">
              <div className="form-action-wrapper py-5">
                <div className="form-group">
                  <h3 className="fs-22 pb-3 fw-bold">Se connecter à First Avocat</h3>
                  <div className="divider">
                    <span />
                  </div>
                  <p className="pt-3">Donnez-nous certaines de vos informations pour accéder gratuitement à First Avocat.</p>
                </div>

                <div className="form-group">
                  <label className="fs-14 text-black fw-medium lh-18" htmlFor="email">
                    Email
                  </label>
                  <input
                    type="email"
                    name="email"
                    className="form-control form--control"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    id="email"
                    autoComplete="email" //
                  />
                </div>
                {/* end form-group */}
                <div className="form-group">
                  <label className="fs-14 text-black fw-medium lh-18" htmlFor="password">
                    Mot de passe
                  </label>
                  <div className="input-group mb-1">
                    <input
                      id="password"
                      className="form-control form--control password-field"
                      type="password"
                      name="password"
                      placeholder="Mot de passe"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      autoComplete="current-password"
                    />
                    <div className="input-group-append">
                      <button className="btn theme-btn-outline theme-btn-outline-gray toggle-password" type="button">
                        <svg className="eye-on" xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 0 24 24" width="22px" fill="#7f8897">
                          <path d="M0 0h24v24H0V0z" fill="none" />
                          <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5C19.17 14.87 15.79 17 12 17s-7.17-2.13-8.82-5.5C4.83 8.13 8.21 6 12 6m0-2C7 4 2.73 7.11 1 11.5 2.73 15.89 7 19 12 19s9.27-3.11 11-7.5C21.27 7.11 17 4 12 4zm0 5c1.38 0 2.5 1.12 2.5 2.5S13.38 14 12 14s-2.5-1.12-2.5-2.5S10.62 9 12 9m0-2c-2.48 0-4.5 2.02-4.5 4.5S9.52 16 12 16s4.5-2.02 4.5-4.5S14.48 7 12 7z" />
                        </svg>
                        <svg className="eye-off" xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 0 24 24" width="22px" fill="#7f8897">
                          <path d="M0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0z" fill="none" />
                          <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5-.59 1.22-1.42 2.27-2.41 3.12l1.41 1.41c1.39-1.23 2.49-2.77 3.18-4.53C21.27 7.11 17 4 12 4c-1.27 0-2.49.2-3.64.57l1.65 1.65C10.66 6.09 11.32 6 12 6zm-1.07 1.14L13 9.21c.57.25 1.03.71 1.28 1.28l2.07 2.07c.08-.34.14-.7.14-1.07C16.5 9.01 14.48 7 12 7c-.37 0-.72.05-1.07.14zM2.01 3.87l2.68 2.68C3.06 7.83 1.77 9.53 1 11.5 2.73 15.89 7 19 12 19c1.52 0 2.98-.29 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45 2.01 3.87zm7.5 7.5l2.61 2.61c-.04.01-.08.02-.12.02-1.38 0-2.5-1.12-2.5-2.5 0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75c-.23.55-.36 1.15-.36 1.78 0 2.48 2.02 4.5 4.5 4.5.63 0 1.23-.13 1.77-.36l.98.98c-.88.24-1.8.38-2.75.38-3.79 0-7.17-2.13-8.82-5.5.7-1.43 1.72-2.61 2.93-3.53z" />
                        </svg>
                      </button>
                    </div>
                  </div>
                  <p className="fs-13 lh-18">Les mots de passe doivent contenir au moins huit caractères, dont au moins 1 lettre et 1 chiffre.</p>
                </div>
                {/* end form-group */}

                {/* end form-group */}
                <div className="form-group">
                  <button id="send-message-btn" className="btn theme-btn w-100" type="submit">
                    Se connecter <i className="la la-arrow-right icon ml-1" />
                  </button>
                </div>
                {/* end form-group */}
                <p className="fs-13 lh-18 pb-3">
                  En cliquant sur « S'inscrire », vous acceptez notre{" "}
                  <a href="terms-and-conditions.html" className="text-color hover-underline">
                    conditions générales
                  </a>
                  ,{" "}
                  <a href="privacy-policy.html" className="text-color hover-underline">
                    politique de confidentialité
                  </a>
                </p>
                <div className="social-icon-box">
                  <div className="pb-3 d-flex align-items-center">
                    <hr className="flex-grow-1 border-top-gray" />
                    <span className="mx-2 text-gray-2 fw-medium text-uppercase fs-14">ou</span>
                    <hr className="flex-grow-1 border-top-gray" />
                  </div>
                  <button className="btn theme-btn google-btn d-flex align-items-center justify-content-center w-100 mb-2" type="button">
                    <span className="btn-icon">
                      <svg focusable="false" width="16px" height="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488 512">
                        <path
                          fill="currentColor"
                          d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"
                        />
                      </svg>
                    </span>
                    <Link to={`${process.env.REACT_APP_API}auth/google`}>
                      <span className="flex-grow-1">Inscrivez-vous avec Google</span>
                    </Link>
                  </button>
                </div>
              </div>
              {/* end form-action-wrapper */}
            </div>
            {/* end col-lg-5 */}
          </div>
        </form>

        {/* end row */}

        <p className="text-black text-center fs-15">
          Vous avez oublié ?{" "}
          <Link to="/forget_password" className="text-color hover-underline">
            Mot de passe oublié
          </Link>
        </p>
      </div>
      {/* end container */}
      <div className="position-absolute top-0 left-0 w-100 h-100 z-index-n1">
        <svg className="w-100 h-100" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
          <path d="M1200 120L0 16.48 0 0 1200 0 1200 120z" fill="#2d86eb" opacity="0.06" />
        </svg>
      </div>
    </section>
  );
}

export default LoginPage;
