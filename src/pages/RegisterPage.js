import React, { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useToken } from "../auth/useToken";
import { inscription } from "../services/userApi";
import { GlobalContext } from "../context/Global";

function RegisterPage() {
  const navigate = useNavigate();
  const { token, setToken } = useContext(GlobalContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phone, setPhone] = useState("");

  const handleRegister = (e) => {
    e.preventDefault();
    if (email != null || password != null) {
      inscription(firstName, lastName, email, password, phone)
        .then((res) => {
          const token = res.data.token;
          setToken(token);
          setTimeout(() => {
            //window.location.reload(false);
            navigate("/");
          }, 100);
        })
        .catch(() => {
          alert("email déja utilisé");
        });
    } else {
      alert("veuillez remplir tous les champs");
    }
  };
  return (
    <div>
      <section className="hero-area bg-white shadow-sm pt-80px pb-80px">
        <span className="icon-shape icon-shape-1" />
        <span className="icon-shape icon-shape-2" />
        <span className="icon-shape icon-shape-3" />
        <span className="icon-shape icon-shape-4" />
        <span className="icon-shape icon-shape-5" />
        <span className="icon-shape icon-shape-6" />
        <span className="icon-shape icon-shape-7" />
        <div className="container">
          <div className="hero-content text-center">
            <h2 className="section-title pb-3 fs-40">Get started</h2>
            <p className="section-desc fs-21">Collaborate, connect, and grow your team’s wisdom</p>
          </div>
          {/* end hero-content */}
        </div>
        {/* end container */}
      </section>
      {/*======================================
  END HERO AREA
======================================*/}
      {/*======================================
  START CONTACT AREA
======================================*/}
      <section className="contact-area pt-80px pb-120px position-relative">
        <div className="container">
          <form action="#" className="contact-form col-lg-10 mx-auto" onSubmit={(e) => handleRegister(e)}>
            <div className="row">
              <div className="col-lg-5">
                <div className="contact-information-wrap bg-white pt-60px">
                  <span className="badge bg-5 text-white fs-14">Free plan</span>
                  <p className="pt-2 pb-5">Free knowledge sharing and collaboration platform</p>
                  <ul className="generic-list-item fs-15">
                    <li className="mb-3">
                      <div className="icon-element icon-element-xs shadow-sm d-inline-block mr-2">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#48a868">
                          <path d="M0 0h24v24H0V0z" fill="none" />
                          <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
                        </svg>
                      </div>{" "}
                      No credit card required
                    </li>
                    <li className="mb-3">
                      <div className="icon-element icon-element-xs shadow-sm d-inline-block mr-2">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#48a868">
                          <path d="M0 0h24v24H0V0z" fill="none" />
                          <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
                        </svg>
                      </div>{" "}
                      Free for up to 50 members
                    </li>
                    <li className="mb-3">
                      <div className="icon-element icon-element-xs shadow-sm d-inline-block mr-2">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#48a868">
                          <path d="M0 0h24v24H0V0z" fill="none" />
                          <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
                        </svg>
                      </div>{" "}
                      Save and share information with your whole team
                    </li>
                    <li className="mb-3">
                      <div className="icon-element icon-element-xs shadow-sm d-inline-block mr-2">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#48a868">
                          <path d="M0 0h24v24H0V0z" fill="none" />
                          <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
                        </svg>
                      </div>{" "}
                      Search for answers when you need them most
                    </li>
                  </ul>
                </div>
              </div>
              {/* end col-lg-5 */}
              <div className="col-lg-6 ml-auto">
                <div className="card card-item">
                  <div className="card-body">
                    <div className="social-icon-box pb-3">
                      <button className="btn theme-btn google-btn d-flex align-items-center justify-content-center w-100 mb-2" type="button">
                        <span className="btn-icon">
                          <svg focusable="false" width="16px" height="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488 512">
                            <path
                              fill="currentColor"
                              d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"
                            />
                          </svg>
                        </span>
                        <Link to={`${process.env.REACT_APP_API}auth/google`}>
                          <span className="flex-grow-1">Sign up with Google</span>
                        </Link>
                      </button>
                    </div>
                    <div className="d-flex align-items-center">
                      <hr className="flex-grow-1 border-top-gray" />
                      <span className="mx-2 text-gray-2 fw-medium text-uppercase fs-14">ou</span>
                      <hr className="flex-grow-1 border-top-gray" />
                    </div>
                    <div className="form-group">
                      <label className="fs-14 text-black fw-medium lh-20" htmlFor="nom">
                        Nom
                      </label>
                      <input
                        type="text"
                        name="name"
                        className="form-control form--control fs-14"
                        placeholder="e.g smith"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        id="nom"
                        autoComplete="nom" //
                      />
                    </div>
                    <div className="form-group">
                      <label className="fs-14 text-black fw-medium lh-20" htmlFor="prenom">
                        Prénom
                      </label>
                      <input
                        type="text"
                        name="name"
                        className="form-control form--control fs-14"
                        placeholder="e.g Fred "
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        id="prenom"
                        autoComplete="prenom" //
                      />
                    </div>
                    <div className="form-group">
                      <label className="fs-14 text-black fw-medium lh-20" htmlFor="phone">
                        Numéro de téléphone
                      </label>
                      <input
                        type="text"
                        name="phone"
                        className="form-control form--control fs-14"
                        placeholder="e.g. +33XXXXXXXXX"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        id="phone"
                        autoComplete="phone" //
                      />
                    </div>
                    {/* end form-group */}
                    <div className="form-group">
                      <label className="fs-14 text-black fw-medium lh-20" htmlFor="email">
                        Email
                      </label>
                      <input
                        type="email"
                        name="email"
                        className="form-control form--control fs-14"
                        placeholder="e.g. fredsmith@gmail.com"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        id="email"
                        autoComplete="email" //
                      />
                    </div>
                    {/* end form-group */}
                    <div className="form-group">
                      <label className="fs-14 text-black fw-medium lh-20" htmlFor="password">
                        Mot de passe
                      </label>
                      <div className="input-group">
                        <input
                          className="form-control form--control password-field"
                          type="password"
                          name="password"
                          placeholder="Enter password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                          id="password"
                          autoComplete="current-password" //
                        />
                        <div className="input-group-append">
                          <button className="btn theme-btn-outline theme-btn-outline-gray toggle-password" type="button">
                            <svg className="eye-on" xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 0 24 24" width="22px" fill="#7f8897">
                              <path d="M0 0h24v24H0V0z" fill="none" />
                              <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5C19.17 14.87 15.79 17 12 17s-7.17-2.13-8.82-5.5C4.83 8.13 8.21 6 12 6m0-2C7 4 2.73 7.11 1 11.5 2.73 15.89 7 19 12 19s9.27-3.11 11-7.5C21.27 7.11 17 4 12 4zm0 5c1.38 0 2.5 1.12 2.5 2.5S13.38 14 12 14s-2.5-1.12-2.5-2.5S10.62 9 12 9m0-2c-2.48 0-4.5 2.02-4.5 4.5S9.52 16 12 16s4.5-2.02 4.5-4.5S14.48 7 12 7z" />
                            </svg>
                            <svg className="eye-off" xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 0 24 24" width="22px" fill="#7f8897">
                              <path d="M0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0z" fill="none" />
                              <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5-.59 1.22-1.42 2.27-2.41 3.12l1.41 1.41c1.39-1.23 2.49-2.77 3.18-4.53C21.27 7.11 17 4 12 4c-1.27 0-2.49.2-3.64.57l1.65 1.65C10.66 6.09 11.32 6 12 6zm-1.07 1.14L13 9.21c.57.25 1.03.71 1.28 1.28l2.07 2.07c.08-.34.14-.7.14-1.07C16.5 9.01 14.48 7 12 7c-.37 0-.72.05-1.07.14zM2.01 3.87l2.68 2.68C3.06 7.83 1.77 9.53 1 11.5 2.73 15.89 7 19 12 19c1.52 0 2.98-.29 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45 2.01 3.87zm7.5 7.5l2.61 2.61c-.04.01-.08.02-.12.02-1.38 0-2.5-1.12-2.5-2.5 0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75c-.23.55-.36 1.15-.36 1.78 0 2.48 2.02 4.5 4.5 4.5.63 0 1.23-.13 1.77-.36l.98.98c-.88.24-1.8.38-2.75.38-3.79 0-7.17-2.13-8.82-5.5.7-1.43 1.72-2.61 2.93-3.53z" />
                            </svg>
                          </button>
                        </div>
                      </div>
                      <p className="fs-13 pt-2 lh-18">
                        Votre mot de passe doit comporter au moins 6 caractères et doit contenir des lettres, des chiffres et des caractères spéciaux.
                        Ne peut pas contenir d'espaces.
                      </p>
                    </div>
                    {/* end form-group */}

                    {/* end form-group */}

                    {/* end form-group */}
                    <div className="form-group">
                      <button className="btn theme-btn w-100" type="submit">
                        S'enregistrer
                      </button>
                    </div>
                    {/* end form-group */}
                    <p className="fs-13 lh-18">
                      En cliquant sur « S'inscrire », vous acceptez notre{" "}
                      <a href="terms-and-conditions.html" className="text-color hover-underline">
                        conditions générales
                      </a>
                      ,{" "}
                      <a href="privacy-policy.html" className="text-color hover-underline">
                        politique de confidentialité
                      </a>
                    </p>
                  </div>
                </div>
                <p className="text-black text-center fs-15">
                  Vous avez déjà un compte?{" "}
                  <a href="login.html" className="text-color hover-underline">
                    Se connecter
                  </a>
                </p>
              </div>
              {/* end col-lg-6 */}
            </div>
          </form>

          {/* end row */}

          <svg
            className="position-absolute bottom-0 z-index-n1 w-100 left-0"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            fill="none"
            height={506}
            viewBox="0 0 1373 506"
            width={1373}
          >
            <linearGradient id="a" gradientUnits="userSpaceOnUse" x1="1464.06" x2="1124.2" y1="829.034" y2="391.259">
              <stop offset={0} stopColor="#4ca143" />
              <stop offset={1} stopColor="#a9d9a4" />
            </linearGradient>
            <linearGradient id="b">
              <stop offset={0} stopColor="#3f3f3f" />
              <stop offset={1} stopColor="#8c8c8c" />
            </linearGradient>
            <linearGradient id="c" gradientUnits="userSpaceOnUse" x1="1275.8" x2="1335.9" xlinkHref="#b" y1="483.96" y2="452.982" />
            <linearGradient id="d" gradientUnits="userSpaceOnUse" x1="92.3715" x2="27.17" y1="573.291" y2="570.061">
              <stop offset={0} stopColor="#fde476" />
              <stop offset=".9944" stopColor="#fbba23" />
            </linearGradient>
            <linearGradient id="e">
              <stop offset={0} stopColor="#99d4ff" />
              <stop offset={1} stopColor="#3af" />
            </linearGradient>
            <linearGradient id="f" gradientUnits="userSpaceOnUse" x1={89} x2={82} xlinkHref="#e" y1="485.696" y2="439.196" />
            <linearGradient id="g" gradientUnits="userSpaceOnUse" x1="29.4999" x2="84.3793" xlinkHref="#e" y1="420.697" y2="484.235" />
            <linearGradient id="h" gradientUnits="userSpaceOnUse" x1="126.012" x2="82.2964" xlinkHref="#b" y1="455.084" y2="441.51" />
            <path d="m105.135 335.775c-39.3334-70.667-88.2-222.2 31-263.0003 149-51 366 326.0003 550 208.0003s623.995-165 413.995-195.0003c-209.995-30 314 135.0003 196 260.0003" />
            <path
              d="m53.0831 91.3989c3.2077 0 6.3472.4817 6.8249 1.3764.546.8947 1.092-.9635 1.092-4.1292 0-7.4326-.4095-7.4326-.4095-14.934 0-.757-.1365-1.5141-.4095-2.2023s-.6825-1.3076-1.2285-1.8581c-.546-.5506-1.1602-.9635-1.8427-1.2388s-1.4332-.4129-2.184-.4129c-5.5282 0-5.5282.1376-10.9881.1376-.7508 0-1.4332.1377-2.1157.413-.6825.2752-1.2968.6882-1.8428 1.2387-.546.5506-.9555 1.17-1.2285 1.8582s-.4095 1.4452-.4095 2.1334c0 5.8497-.3412 5.8497-.3412 11.6994 0 1.5829.6825 3.0281 1.7745 4.1293 1.092 1.1011 2.5935 1.7205 4.1632 1.7205 4.5727 0 4.5727.0688 9.1454.0688z"
              fill="#c8ccd0"
              opacity=".5"
            />
            <path
              d="m1326.92 397.314c-.57-2.37-1.95-3.931-3.48-5.814s-3.17-4.039-5.35-5.06c-3.73-1.555-2.34-1.112-5.65-3.44 0 0 6.7-12.448 13.06-8.26 6.35 4.188 1.42 22.574 1.42 22.574z"
              fill="#1060e1"
            />
            <path
              clipRule="evenodd"
              d="m1298.4 425.808c.28 1.981 1.12 3.854 2.44 5.373 1.76 2.034 4.25 3.268 6.91 3.43s5.28-.761 7.27-2.566c1.79-1.621 2.95-3.835 3.28-6.237h11.08c2.74 0 5.31.873 7.19 2.62 2.62 1.699 4.88 4.387 5.65 7.511l10.61 36.854c1.37 1.572 2.22 3.668 2.05 5.939-.17 3.318-2.22 6.113-4.96 7.685l-7.87 2.096-68.28.524-10.96-2.969c-2.73-1.223-4.62-4.018-4.79-7.336-.17-2.271.69-4.192 1.88-5.764 1.29-4.402 2.64-8.801 3.99-13.205 2.67-8.659 5.33-17.336 7.48-26.095 1.88-3.668 5.13-5.939 8.73-6.637 1.54-.699 3.25-1.223 5.13-1.223z"
              fill="url(#a)"
              fillRule="evenodd"
            />
            <path d="m1271 459c0-3.314 2.69-6 6-6h61c3.31 0 6 2.686 6 6v44h-73z" fill="url(#c)" />
            <path
              d="m1307.59 482.187c2.54 0 4.6-2.056 4.6-4.593s-2.06-4.594-4.6-4.594c-2.53 0-4.59 2.057-4.59 4.594s2.06 4.593 4.59 4.593z"
              fill="#e4e6e8"
            />
            <path
              d="m1286.43 410.291c1.01-.291 11.52-21.306 21.76-25.103 10.24-3.798 12.3-9.204 12.3-9.204s-8.62-12.514-22.54-4.04-12.53 38.638-11.52 38.347z"
              fill="#1060e1"
            />
            <path
              clipRule="evenodd"
              d="m1292.27 397.058c0 2.759.89 4.995 1.99 4.996.11 4.26.3 7.617.63 8.506 1.04 2.838 2.6 5.456 4.6 7.72l-1.05 7.22c0 2 .2 3.966 2 6 1.81 2.034 2.95 3.228 7.5 3.5 2.72.162 6.22-1.149 8.26-2.954 2.03-1.805 3.26-4.344 3.43-7.059l.33-5.362c1.55-.816 2.87-2.013 3.83-3.481.96-1.467 1.53-3.154 1.65-4.903l.1-8.692c.09.031.18.046.27.046 1.1 0 1.99-2.236 1.99-4.995s-.89-4.996-1.99-4.996c-.31 0-.6.175-.86.486-.55-1.252-1.28-2.427-2.15-3.491-1.44-1.751-3.26-3.153-5.32-4.103-2.01-.827-3.96-1.777-5.85-2.843-1 .504-2.11.996-3.36 1.463-4.16 1.558-8.55 6.272-12.16 11.068-.3-1.83-1.01-3.121-1.85-3.121-1.1 0-1.99 2.236-1.99 4.995z"
              fill="#f5d0a7"
              fillRule="evenodd"
            />
            <path d="m1303.99 413.284c-.07 1.345 2.9 2.585 6.66 2.795 3.75.192 6.85-1.45 6.91-2.795z" fill="#fff" />
            <rect fill="#c8ccd0" height="2.35948" opacity=".5" rx="1.17974" width="114.435" x={1251} y={503} />
            <path d="m70.3069 454.84 12.1129-6.562 13.3989 29.043c-2.379 6-8.3794 4.766-8.3794 4.766z" fill="#c69c6d" />
            <path
              clipRule="evenodd"
              d="m30.5136 502.696h51.6989c.1298-6.038-1.0717-12.035-3.5248-17.563l-35.1802-2.26-8.4985.532c-3.4879 2.491-4.1102 11.671-4.4954 19.291z"
              fill="url(#d)"
              fillRule="evenodd"
            />
            <path
              d="m85.1319 470.994-.1899-.201 27.798-26.149 6.916-9.135c2.806-2.93 5.708-3.907 7.749-1.951 2.042 1.957 3.269 4.716.452 7.635l-10.13 8.183-20.9394 28.53c-.3322.976-.8783 1.866-1.5985 2.604-.7201.738-1.5962 1.306-2.5641 1.663-.9678.356-2.003.491-3.0299.396-1.027-.095-2.0197-.418-2.9056-.946-.886-.528-1.6427-1.247-2.215-2.105s-.9457-1.833-1.0928-2.854c-.1472-1.021-.0644-2.062.2423-3.046.3067-.985.8295-1.889 1.5303-2.646z"
              fill="#c69c6d"
            />
            <path d="m88.569 462.696-10.2641-28-3.2658 30c3.4213 6.833 10.7306 20.6 12.5968 21s1.3996-15.167.9331-23z" fill="url(#f)" />
            <path
              d="m63.4366 362.083-3.2939-.083-.841 54.572 9.4612.165c8.1998.166 14.6475-7.547 13.9466-16.67l-1.4717-19.407c-.771-10.284-8.4801-18.328-17.8012-18.577z"
              fill="#220903"
            />
            <path
              d="m31.2542 425.257c.0875-.237.1999-.463.3353-.675.1117-.228.2386-.447.3799-.658.201-.316.455-.595.7504-.823 1.0152-.934 2.33-1.459 3.6973-1.477l34.5963.884c5.3205-.105 9.5698 5.868 9.4102 11.224l-.1289 5.65.9019 4.655c.8039 1.424 1.2435 3.031 1.2786 4.674.0203 1.265-.2046 2.522-.662 3.699-.4573 1.177-1.1381 2.251-2.0034 3.161 0 0-.3098 29.125-1.3098 29.625s-44-.5-44.5-1 1.9735-12.461 1.9735-12.461l-10.892-1.124s1.9306-35.621 6.1727-45.354z"
              fill="url(#g)"
            />
            <circle cx="107.039" cy="446.696" fill="#fff" r={6} />
            <path
              d="m89.9215 424.347c.1651-.826.991-1.651 1.872-1.651h35.7335c.991 0 1.652.66 1.487 1.651l-6.883 49.003c-.165.826-.991 1.652-1.872 1.652h-35.7334c-.991 0-1.4866-.661-1.4866-1.652zm14.9215 25.548c.66 1.872 2.202 2.037 3.358.496 1.157-1.487 1.652-4.185.991-5.892-.66-1.872-2.202-2.037-3.358-.495-1.322 1.376-1.652 4.019-.991 5.891z"
              fill="url(#h)"
            />
            <path
              d="m48.6415 390.524c.5314.025 1.05.172 1.5157.429l-.9449-14.432 30.2982 2.039-2.2383 21.297c-.0063 6.332-5.8005 11.301-12.6826 10.929l-.1511 8.015s-.3316 12.279-10.6754 8.57c0 0-6.6896-1.469-5.8089-4.976 1.2386-5.261 2.195-10.585 2.8649-15.949.0356-.287.1275-.564.2708-.816l-.4995-7.909-2.3611-.117c-.4825-.001-.9594-.102-1.4016-.295s-.84-.475-1.169-.828c-.3291-.353-.5824-.77-.744-1.224-.1617-.455-.2284-.938-.1962-1.419.0323-.482.1629-.952.3838-1.381s.5273-.808.9005-1.114.8052-.532 1.2692-.664c.464-.133.9502-.169 1.4286-.106z"
              fill="#c69c6d"
            />
            <path d="m72.7162 403.074c-.0957 1.932-2.9873 3.218-6.2897 2.87-3.3031-.333-5.8245-2.179-5.6828-4.108z" fill="#fff" />
            <path d="m57.3145 388.542-9.258 1.985" stroke="#000" strokeMiterlimit={10} strokeWidth=".383746" />
            <path d="m129.039 440.196c0 2.209-.5 5-2.5 5s-1.5-2.291-1.5-4.5c0-2.21-.5-4 1.5-4s2.5 1.29 2.5 3.5z" fill="#c69c6d" />
            <path
              d="m35.9557 403.816 3.3898-24.243c1.4528-10.221 10.4922-17.739 21.0651-17.57l2.5827.084-.0807 3.97 5.9725.085c4.2776.084 10.8958 2.956 13.2363 6.335 1.6142 2.365-.3228 4.562-.4035 7.687l-.0808 5.998c-5.5689-.085-10.815-2.788-14.2048-7.434l-4.6811-6.166-.0807 3.801c-.0807 3.886-1.2106 7.602-3.3898 10.812l-4.8425 7.434c-4.439 6.757-4.5198 15.373-.1615 22.131l-7.1831-.169c-6.7796-.084-12.1064-6.082-11.1379-12.755z"
              fill="#220903"
            />
            <path
              d="m31.2016 464.276.0136-.276 50.2487.138.5221.01 14.9151-.152c4.0569.001 1.8285 2.488 1.8268 5.315-.0018 2.828-2.4567 4.92-6.5132 4.903l-10.9306-2.724-47.0171 5.986c-.935.435-1.9555.656-2.9867.646-1.0313-.01-2.0475-.25-2.9741-.703-.9265-.453-1.7402-1.107-2.3815-1.915-.6413-.807-1.0941-1.748-1.3252-2.753-.2312-1.005-.2347-2.049-.0106-3.056.2242-1.007.6703-1.951 1.306-2.763s1.4449-1.472 2.3683-1.931 1.9379-.706 2.9691-.723z"
              fill="#c69c6d"
            />
            <g fill="#c8ccd0">
              <rect height="2.35948" opacity=".5" rx="1.17974" width="114.435" y="502.696" />
              <path
                d="m116.389 386.389-9 9v-42c0-3.3 2.7-6 6-6h36c1.591 0 3.117.632 4.243 1.757 1.125 1.125 1.757 2.652 1.757 4.243v27c0 1.591-.632 3.117-1.757 4.243-1.126 1.125-2.652 1.757-4.243 1.757zm23.25-11.91c.96-1.11 1.65-2.25 2.1-3.45.54-1.53.84-3.33.84-5.37 0-3.87-1.05-6.87-3.09-9-1.087-1.09-2.392-1.939-3.829-2.492s-2.974-.798-4.511-.718c-1.547-.081-3.093.163-4.54.716-1.446.553-2.761 1.402-3.86 2.494-2.181 2.472-3.292 5.709-3.09 9 0 3.87 1.05 6.87 3.09 9 1.123 1.101 2.463 1.956 3.936 2.509 1.472.553 3.044.791 4.614.701 1.86 0 3.6-.33 5.13-1.02 1.95 1.32 3 2.04 3.18 2.1.69.39 1.38.69 2.1.9l1.77-3.39c-1.366-.479-2.658-1.145-3.84-1.98zm-3.81-2.7c-1.364-1.033-2.883-1.843-4.5-2.4l-1.35 2.7c.99.36 1.98.87 2.94 1.5-.6.21-1.26.33-1.95.33-1.83 0-3.36-.69-4.56-2.04-1.2-1.38-1.8-3.45-1.8-6.21 0-2.7.6-4.74 1.8-6.12.588-.662 1.315-1.185 2.13-1.533.814-.347 1.695-.51 2.58-.477.89-.038 1.777.123 2.596.471.82.347 1.553.873 2.144 1.539 1.2 1.35 1.8 3.39 1.8 6.12 0 1.32-.15 2.49-.48 3.51-.3 1.02-.75 1.89-1.35 2.61z"
                opacity=".5"
              />
              <path
                d="m1274 354h-33c-1.59 0-3.12-.632-4.24-1.757-1.13-1.126-1.76-2.652-1.76-4.243v-27c0-3.3 2.7-6 6-6h36c1.59 0 3.12.632 4.24 1.757 1.13 1.126 1.76 2.652 1.76 4.243v42zm-3.06-9-9.48-24h-5.04l-9.18 24h5.04l1.95-5.37h9.45l2.07 5.37zm-8.79-9.36h-6.45l3.18-8.76z"
                opacity=".5"
              />
              <path
                clipRule="evenodd"
                d="m1248 112v1h-14v-1l.73-.58c.77-.77.81-3.55 1.19-5.42.77-3.77 4.08-5 4.08-5 0-.55.45-1 1-1s1 .45 1 1c0 0 3.39 1.23 4.16 5 .38 1.88.42 4.66 1.19 5.42l.66.58zm-7 4c1.11 0 2-.89 2-2h-4c0 1.11.89 2 2 2z"
                fillRule="evenodd"
                opacity=".5"
              />
              <path
                clipRule="evenodd"
                d="m124.06 103c-5.06 0-8.06 6-8.06 6s3 6 8.06 6c4.94 0 7.94-6 7.94-6s-3-6-7.94-6zm-.06 10c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4zm0-2c1.11 0 2-.89 2-2s-.89-2-2-2-2 .89-2 2 .89 2 2 2z"
                fillRule="evenodd"
                opacity=".5"
              />
              <g opacity=".5">
                <path
                  clipRule="evenodd"
                  d="m33 217c-.5523 0-1 .448-1 1v.375l-9.7022 3.638c-.7807.293-1.2978 1.039-1.2978 1.873v2.228c0 .722.3884 1.379 1 1.732v1.654c0 1.933 1.567 3.5 3.5 3.5 1.6034 0 2.9549-1.078 3.3693-2.549l3.1307 1.174v.375c0 .552.4477 1 1 1s1-.448 1-1v-14c0-.552-.4477-1-1-1zm-9.4 11.475 3.7609 1.41c-.1781.865-.9436 1.515-1.8609 1.515-1.0493 0-1.9-.851-1.9-1.9z"
                  fillRule="evenodd"
                />
                <path d="m19 222c-.5523 0-1 .448-1 1v4c0 .552.4477 1 1 1s1-.448 1-1v-4c0-.552-.4477-1-1-1z" />
              </g>
              <path
                d="m10.5769 139.115c0-.616-.4994-1.115-1.11536-1.115-.61601 0-1.11539.499-1.11539 1.115v2.229h-2.23076c-.61601 0-1.11539.499-1.11539 1.115 0 .615.49938 1.114 1.11539 1.114h2.23076v2.23c0 .615.49938 1.114 1.11539 1.114.61596 0 1.11536-.499 1.11536-1.114v-2.23h2.2308c.616 0 1.1154-.499 1.1154-1.114 0-.616-.4994-1.115-1.1154-1.115h-2.2308z"
                opacity=".5"
              />
              <path
                d="m1287.58 1.11465c0-.615605-.5-1.11465-1.12-1.11465-.61 0-1.11.499045-1.11 1.11465v2.22929h-2.23c-.62 0-1.12.49905-1.12 1.11465s.5 1.11464 1.12 1.11464h2.23v2.22929c0 .61561.5 1.11464 1.11 1.11464.62 0 1.12-.49903 1.12-1.11464v-2.22929h2.23c.61 0 1.11-.49904 1.11-1.11464s-.5-1.11465-1.11-1.11465h-2.23z"
                opacity=".5"
              />
              <path
                clipRule="evenodd"
                d="m132.464 262.729-4.732-4.729-4.732 4.729 4.732 4.729zm-4.732 1.576-1.577-1.576 1.577-1.576 1.578 1.576z"
                fillRule="evenodd"
                opacity=".5"
              />
              <path
                clipRule="evenodd"
                d="m1372.46 250.729-4.73-4.729-4.73 4.729 4.73 4.729zm-4.73 1.576-1.58-1.576 1.58-1.576 1.58 1.576z"
                fillRule="evenodd"
                opacity=".5"
              />
            </g>
          </svg>
        </div>
        {/* end container */}
      </section>
    </div>
  );
}

export default RegisterPage;
