import React from "react";

function NotFound() {
  return (
    <section className="error-area section-padding position-relative">
      <span className="icon-shape icon-shape-1" />
      <span className="icon-shape icon-shape-2" />
      <span className="icon-shape icon-shape-3" />
      <span className="icon-shape icon-shape-4" />
      <span className="icon-shape icon-shape-5" />
      <span className="icon-shape icon-shape-6" />
      <span className="icon-shape icon-shape-7" />
      <div className="container">
        <div className="media align-items-center justify-content-center error-content">
          <img src="images/404.svg" alt="error-image" className="img-fluid mr-5 error-content-img" />
          <div className="media-body flex-inherit">
            <h2 className="section-title pb-3">Page not found!</h2>
            <p className="section-desc pb-4">We're sorry, we couldn't find the page you requested.</p>
            <ul className="generic-list-item">
              <li>
                Try{" "}
                <a href="#" className="d-inline-block text-color hover-underline">
                  searching for similar questions
                </a>
              </li>
              <li>
                Browse{" "}
                <a href="questions-layout-2.html" className="d-inline-block text-color hover-underline">
                  our recent questions
                </a>
              </li>
              <li>
                Browse{" "}
                <a href="tags-list.html" className="d-inline-block text-color hover-underline">
                  our popular tags
                </a>
              </li>
              <li>
                If you feel something is missing that should be here,{" "}
                <a href="contact.html" className="d-inline-block text-color hover-underline">
                  contact us
                </a>
                .
              </li>
            </ul>
          </div>
        </div>
      </div>
      {/* end container */}
    </section>
  );
}

export default NotFound;
