import React, { useEffect, useState } from "react";
import { useUser } from "../../auth/useUser";
import { getHistoryByUserId } from "../../services/historyApi";

function ListQuestion() {
  const [currentData, setcurrentData] = useState([]);
  const [allInitialData, setallInitialData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [data, setdata] = useState([]);
  const [size, setsize] = useState(5);
  const user = useUser();
  // State to keep track of the currently open accordion item
  const [openIndex, setOpenIndex] = useState(0); // Set first item as open by default

  const handleClick = (event, index) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenIndex(openIndex === index ? -1 : index);
  };
  const handleChangeSize = (value) => {
    setsize(value);
    getData();
  };

  const getData = () => {
    setLoading(true);
    getHistoryByUserId(user.user._id)
      .then((res) => {
        console.log(res);
        setdata(res.data);
        setallInitialData(res.data);
      })
      .catch((error) => setError(error))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    getData();
  }, []);

  const onChangePage = (pageOfItems) => {
    setcurrentData(pageOfItems);
  };
  return (
    <div>
      <section className="hero-area bg-white shadow-sm pt-80px pb-80px">
        <span className="icon-shape icon-shape-1" />
        <span className="icon-shape icon-shape-2" />
        <span className="icon-shape icon-shape-3" />
        <span className="icon-shape icon-shape-4" />
        <span className="icon-shape icon-shape-5" />
        <span className="icon-shape icon-shape-6" />
        <span className="icon-shape icon-shape-7" />
        <div className="container">
          <div className="hero-content text-center">
            <h2 className="section-title pb-3">Frequently Asked Questions</h2>
            <p className="section-desc">
              Find out everything you need to get started by taking the tour. <br />
              If you still have questions contact us:
              <a href="mailto:example@yourwebsite.com" className="text-color hover-underline">
                example@yourwebsite.com
              </a>
            </p>
          </div>
          {/* end hero-content */}
        </div>
        {/* end container */}
      </section>
      <section className="faq-area pt-80px pb-80px">
        <div className="container">
          <div className="row">
            {loading && <p>loading ...</p>}
            {!loading && error && <p>error</p>}
            {!loading && !error && data && (
              <div className="col-lg-12">
                <div id="accordion" className="generic-accordion">
                  {data.map((el, idx) => (
                    <div className="card" key={idx}>
                      <div className="card-header" id={`heading${idx}`}>
                        <button
                          className="btn btn-link"
                          data-toggle="collapse"
                          data-target={`#collapse${idx}`}
                          aria-expanded={openIndex === idx}
                          aria-controls={`collapse${idx}`}
                          onClick={(e) => handleClick(e, idx)}
                        >
                          <span>{el.question}</span>
                          <i className={"la la-angle-down collapse-icon" + (openIndex === idx ? " active" : "")} />
                        </button>
                      </div>
                      <div
                        id={`collapse${idx}`}
                        className={`collapse${openIndex === idx ? " show" : ""}`}
                        aria-labelledby={`heading${idx}`}
                        data-parent="#accordion"
                      >
                        <div className="card-body">
                          <p className="fs-15 lh-24">{el.response}</p>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}

            {/* end col-lg-4 */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </section>
    </div>
  );
}

export default ListQuestion;
