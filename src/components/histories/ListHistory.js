import React, { useEffect, useState } from "react";
import { useUser } from "../../auth/useUser";
import { getHistoryByUserId } from "../../services/historyApi";

function ListHistory() {
  const [currentData, setcurrentData] = useState([]);
  const [allInitialData, setallInitialData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [data, setdata] = useState([]);
  const [size, setsize] = useState(5);
  const user = useUser();

  const handleChangeSize = (value) => {
    setsize(value);
    getData();
  };

  const getData = () => {
    setLoading(true);
    getHistoryByUserId(user.user._id)
      .then((res) => {
        console.log(res);
        setdata(res.data);
        setallInitialData(res.data);
      })
      .catch((error) => setError(error))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    getData();
  }, []);

  const onChangePage = (pageOfItems) => {
    setcurrentData(pageOfItems);
  };

  return (
    <div>
      <section className="hero-area bg-white shadow-sm pt-80px pb-80px">
        <span className="icon-shape icon-shape-1" />
        <span className="icon-shape icon-shape-2" />
        <span className="icon-shape icon-shape-3" />
        <span className="icon-shape icon-shape-4" />
        <span className="icon-shape icon-shape-5" />
        <span className="icon-shape icon-shape-6" />
        <span className="icon-shape icon-shape-7" />
        <div className="container">
          <div className="hero-content text-center">
            <h2 className="section-title pb-3">Wishlist</h2>
            <ul className="breadcrumb-list">
              <li>
                <a href="#">Home</a>
                <span>
                  <svg xmlns="http://www.w3.org/2000/svg" height="19px" viewBox="0 0 24 24" width="19px" fill="#000000">
                    <path d="M0 0h24v24H0V0z" fill="none" />
                    <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6-6-6z" />
                  </svg>
                </span>
              </li>
              <li>Wishlist</li>
            </ul>
          </div>
          {/* end hero-content */}
        </div>
        {/* end container */}
      </section>
      {/*======================================
  END HERO AREA
======================================*/}
      {/*======================================
  START WISHLIST AREA
======================================*/}
      {loading && <p>loading ...</p>}
      {!loading && error && <p>error</p>}
      {!loading && !error && data && (
        <section className="wishlist-area pt-80px pb-80px position-relative">
          <div className="container">
            <form action="#" className="cart-form table-responsive px-2">
              <h3 className="fs-22 pb-4 fw-bold">My wishlist</h3>
              <table className="table generic-table">
                <thead>
                  <tr>
                    <th scope="col">Product Name</th>
                    <th scope="col">Unit Price</th>
                    <th scope="col">Stock Status</th>
                    <th scope="col">Action</th>
                    <th scope="col">Remove</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((el, idx) => (
                    <tr key={idx}>
                      <th scope="row">
                        <div className="media media-card align-items-center shadow-none p-0 mb-0 rounded-0 bg-transparent">
                          <a href="#" className="media-img d-block media-img-sm">
                            <img src="images/product-img.jpg" alt="Product image" />
                          </a>
                          <div className="media-body">
                            <h5 className="fs-15 fw-medium">
                              <a href="#">Chocolate bar</a>
                            </h5>
                          </div>
                        </div>
                      </th>
                      <td>$22</td>
                      <td>In Stock</td>
                      <td>
                        <a href="#" className="btn theme-btn theme-btn-sm">
                          Add to Cart
                        </a>
                      </td>
                      <td>
                        <a href="#" className="icon-element icon-element-xs shadow-sm" data-toggle="tooltip" data-placement="top" title="Remove item">
                          <i className="la la-times" />
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </form>
          </div>
          {/* end container */}
        </section>
      )}
    </div>
  );
}

export default ListHistory;
