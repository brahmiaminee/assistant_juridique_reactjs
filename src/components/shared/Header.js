import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { useUser } from "../../auth/useUser";
import { useToken } from "../../auth/useToken";
import { GlobalContext } from "../../context/Global";
import { parseToken } from "../../utils/SessionUtils";

function Header() {
  const { token, setToken } = useContext(GlobalContext);
  const [user, setUser] = useState(null);
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    const queryParams = new URLSearchParams(location.search);
    const extractedToken = queryParams.get("token");
    if (extractedToken) {
      setToken(extractedToken);
    }
  }, [location]);

  const handleLogout = () => {
    setToken("");
    navigate("/login");
  };

  useEffect(() => {
    if (token != "") {
      setUser(parseToken(token));
    }
  }, [token]);

  return (
    <header className="header-area bg-white border-bottom border-bottom-gray">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-2">
            <div className="logo-box">
              <Link to="/" className="logo">
                <img src="images/logo-black.png" alt="logo" />
              </Link>
              <div className="user-action">
                <div
                  className="off-canvas-menu-toggle icon-element icon-element-xs shadow-sm mr-1"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Main menu"
                >
                  <i className="la la-bars" />
                </div>
                <div
                  className="user-off-canvas-menu-toggle icon-element icon-element-xs shadow-sm"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="User menu"
                >
                  <i className="la la-user" />
                </div>
              </div>
            </div>
          </div>
          {/* end col-lg-2 */}
          <div className="col-lg-10">
            <div className="menu-wrapper border-left border-left-gray pl-4">
              <nav className="menu-bar mr-auto">
                <ul>
                  {token != "" && (
                    <li>
                      <Link to="/history">Historique</Link>
                    </li>
                  )}

                  <li className="is-mega-menu">
                    <Link to="/contact">Contact</Link>
                  </li>
                </ul>
                {/* end ul */}
              </nav>

              {token == "" ? (
                <div className="nav-right-button">
                  <Link to="/login" className="btn theme-btn theme-btn-outline mr-2">
                    <i className="la la-sign-in mr-1" /> Se connecter
                  </Link>
                  <Link to="/register" className="btn theme-btn">
                    <i className="la la-user mr-1" /> Sign up
                  </Link>
                </div>
              ) : (
                <div className="nav-right-button">
                  <ul className="user-action-wrap d-flex align-items-center">
                    <li className="dropdown">
                      <span className="ball red ball-lg noti-dot" />
                      <a
                        className="nav-link dropdown-toggle dropdown--toggle"
                        href="#"
                        id="notificationDropdown"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        <i className="la la-bell" />
                      </a>
                      <div className="dropdown-menu dropdown--menu dropdown-menu-right mt-3 keep-open" aria-labelledby="notificationDropdown">
                        <h6 className="dropdown-header">
                          <i className="la la-bell pr-1 fs-16" />
                          Notifications
                        </h6>
                        <div className="dropdown-divider border-top-gray mb-0" />
                        <div className="dropdown-item-list">
                          <a className="dropdown-item" href="notifications.html">
                            <div className="media media-card media--card shadow-none mb-0 rounded-0 align-items-center bg-transparent">
                              <div className="media-img media-img-sm flex-shrink-0">
                                <img src="images/img3.jpg" alt="avatar" />
                              </div>
                              <div className="media-body p-0 border-left-0">
                                <h5 className="fs-14 fw-regular">John Doe following your post</h5>
                                <small className="meta d-block lh-24">
                                  <span>45 secs ago</span>
                                </small>
                              </div>
                            </div>
                          </a>
                          <a className="dropdown-item" href="notifications.html">
                            <div className="media media-card media--card shadow-none mb-0 rounded-0 align-items-center bg-transparent">
                              <div className="media-img media-img-sm flex-shrink-0">
                                <img src="images/img4.jpg" alt="avatar" />
                              </div>
                              <div className="media-body p-0 border-left-0">
                                <h5 className="fs-14 fw-regular">Arnold Smith answered on your post</h5>
                                <small className="meta d-block lh-24">
                                  <span>5 mins ago</span>
                                </small>
                              </div>
                            </div>
                          </a>
                          <a className="dropdown-item" href="notifications.html">
                            <div className="media media-card media--card shadow-none mb-0 rounded-0 align-items-center bg-transparent">
                              <div className="media-img media-img-sm flex-shrink-0">
                                <img src="images/img4.jpg" alt="avatar" />
                              </div>
                              <div className="media-body p-0 border-left-0">
                                <h5 className="fs-14 fw-regular">Saeed bookmarked your post</h5>
                                <small className="meta d-block lh-24">
                                  <span>1 hour ago</span>
                                </small>
                              </div>
                            </div>
                          </a>
                        </div>
                        <a className="dropdown-item pb-1 border-bottom-0 text-center btn-text fw-regular" href="notifications.html">
                          View All Notifications <i className="la la-arrow-right icon ml-1" />
                        </a>
                      </div>
                    </li>
                    <li className="dropdown user-dropdown">
                      <Link className="nav-link dropdown-toggle dropdown--toggle pl-2" to="/profil">
                        <div className="media media-card media--card shadow-none mb-0 rounded-0 align-items-center bg-transparent">
                          <div className="media-img media-img-xs flex-shrink-0 rounded-full mr-2">
                            <img src="images/img4.jpg" alt="avatar" className="rounded-full" />
                          </div>
                          <div className="media-body p-0 border-left-0">
                            <h5 className="fs-14">
                              {user && user.user.firstName} {user && user.user.lastName}
                            </h5>
                          </div>
                        </div>
                      </Link>
                    </li>
                    <li>
                      <Link to="/login" className="btn theme-btn theme-btn-outline mr-2" onClick={handleLogout}>
                        <i className="la la-sign-in mr-1" /> Logout
                      </Link>
                    </li>
                  </ul>
                </div>
              )}

              {/* end nav-right-button */}
            </div>
            {/* end menu-wrapper */}
          </div>
          {/* end col-lg-10 */}
        </div>
        {/* end row */}
      </div>
      {/* end container */}
      <div className="off-canvas-menu custom-scrollbar-styled">
        <div className="off-canvas-menu-close icon-element icon-element-sm shadow-sm" data-toggle="tooltip" data-placement="left" title="Close menu">
          <i className="la la-times" />
        </div>
        {/* end off-canvas-menu-close */}
        <ul className="generic-list-item off-canvas-menu-list pt-90px">
          <li>
            <a href="#">Home</a>
            <ul className="sub-menu">
              <li>
                <a href="index-2.html">Home - landing</a>
              </li>
              <li>
                <a href="home-2.html">Home - main</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">Pages</a>
            <ul className="sub-menu">
              <li>
                <a href="user-profile.html">user profile</a>
              </li>
              <li>
                <a href="notifications.html">Notifications</a>
              </li>
              <li>
                <a href="referrals.html">Referrals</a>
              </li>
              <li>
                <a href="setting.html">settings</a>
              </li>
              <li>
                <a href="ask-question.html">ask question</a>
              </li>
              <li>
                <a href="question-details.html">question details</a>
              </li>
              <li>
                <a href="about.html">about</a>
              </li>
              <li>
                <a href="revisions.html">revisions</a>
              </li>
              <li>
                <a href="category.html">category</a>
              </li>
              <li>
                <a href="companies.html">companies</a>
              </li>
              <li>
                <a href="company-details.html">company details</a>
              </li>
              <li>
                <a href="careers.html">careers</a>
              </li>
              <li>
                <a href="career-details.html">career details</a>
              </li>
              <li>
                <a href="contact.html">contact</a>
              </li>
              <li>
                <a href="faq.html">FAQs</a>
              </li>
              <li>
                <a href="pricing-table.html">pricing tables</a>
              </li>
              <li>
                <a href="error.html">page 404</a>
              </li>
              <li>
                <a href="terms-and-conditions.html">Terms &amp; conditions</a>
              </li>
              <li>
                <a href="privacy-policy.html">privacy policy</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">blog</a>
            <ul className="sub-menu">
              <li>
                <a href="blog-grid-no-sidebar.html">grid no sidebar</a>
              </li>
              <li>
                <a href="blog-left-sidebar.html">blog left sidebar</a>
              </li>
              <li>
                <a href="blog-right-sidebar.html">blog right sidebar</a>
              </li>
              <li>
                <a href="blog-single.html">blog detail</a>
              </li>
            </ul>
          </li>
        </ul>
        <div className="off-canvas-btn-box px-4 pt-5 text-center">
          <a href="login.html" className="btn theme-btn theme-btn-sm theme-btn-outline">
            <i className="la la-sign-in mr-1" /> Login
          </a>
          <span className="fs-15 fw-medium d-inline-block mx-2">Or</span>
          <a href="signup.html" className="btn theme-btn theme-btn-sm">
            <i className="la la-plus mr-1" /> Sign up
          </a>
        </div>
      </div>
      {/* end off-canvas-menu */}
      <div className="user-off-canvas-menu custom-scrollbar-styled">
        <div
          className="user-off-canvas-menu-close icon-element icon-element-sm shadow-sm"
          data-toggle="tooltip"
          data-placement="left"
          title="Close menu"
        >
          <i className="la la-times" />
        </div>
        {/* end user-off-canvas-menu-close */}
        <ul className="nav nav-tabs generic-tabs generic--tabs pt-90px pl-4 shadow-sm" id="myTab2" role="tablist">
          <li className="nav-item">
            <div className="anim-bar" />
          </li>
          <li className="nav-item">
            <a
              className="nav-link active"
              id="user-notification-menu-tab"
              data-toggle="tab"
              href="#user-notification-menu"
              role="tab"
              aria-controls="user-notification-menu"
              aria-selected="true"
            >
              Notifications
            </a>
          </li>
          <li className="nav-item">
            <a
              className="nav-link"
              id="user-profile-menu-tab"
              data-toggle="tab"
              href="#user-profile-menu"
              role="tab"
              aria-controls="user-profile-menu"
              aria-selected="false"
            >
              Profile
            </a>
          </li>
        </ul>
        <div className="tab-content pt-3" id="myTabContent2">
          <div className="tab-pane fade show active" id="user-notification-menu" role="tabpanel" aria-labelledby="user-notification-menu-tab">
            <div className="dropdown--menu shadow-none w-auto rounded-0">
              <div className="dropdown-item-list">
                <a className="dropdown-item" href="notifications.html">
                  <div className="media media-card media--card shadow-none mb-0 rounded-0 align-items-center bg-transparent">
                    <div className="media-img media-img-sm flex-shrink-0">
                      <img src="images/img3.jpg" alt="avatar" />
                    </div>
                    <div className="media-body p-0 border-left-0">
                      <h5 className="fs-14 fw-regular">John Doe following your post</h5>
                      <small className="meta d-block lh-24">
                        <span>45 secs ago</span>
                      </small>
                    </div>
                  </div>
                </a>
                <a className="dropdown-item" href="notifications.html">
                  <div className="media media-card media--card shadow-none mb-0 rounded-0 align-items-center bg-transparent">
                    <div className="media-img media-img-sm flex-shrink-0">
                      <img src="images/img4.jpg" alt="avatar" />
                    </div>
                    <div className="media-body p-0 border-left-0">
                      <h5 className="fs-14 fw-regular">Arnold Smith answered on your post</h5>
                      <small className="meta d-block lh-24">
                        <span>5 mins ago</span>
                      </small>
                    </div>
                  </div>
                </a>
                <a className="dropdown-item" href="notifications.html">
                  <div className="media media-card media--card shadow-none mb-0 rounded-0 align-items-center bg-transparent">
                    <div className="media-img media-img-sm flex-shrink-0">
                      <img src="images/img4.jpg" alt="avatar" />
                    </div>
                    <div className="media-body p-0 border-left-0">
                      <h5 className="fs-14 fw-regular">Saeed bookmarked your post</h5>
                      <small className="meta d-block lh-24">
                        <span>1 hour ago</span>
                      </small>
                    </div>
                  </div>
                </a>
              </div>
              <a className="dropdown-item border-bottom-0 text-center btn-text fw-regular" href="notifications.html">
                View All Notifications <i className="la la-arrow-right icon ml-1" />
              </a>
            </div>
          </div>
          {/* end tab-pane */}
          <div className="tab-pane fade" id="user-profile-menu" role="tabpanel" aria-labelledby="user-profile-menu-tab">
            <div className="dropdown--menu shadow-none w-auto rounded-0">
              <div className="dropdown-item-list">
                <a className="dropdown-item" href="user-profile.html">
                  <i className="la la-user mr-2" />
                  Profile
                </a>
                <a className="dropdown-item" href="notifications.html">
                  <i className="la la-bell mr-2" />
                  Notifications
                </a>
                <a className="dropdown-item" href="referrals.html">
                  <i className="la la-user-plus mr-2" />
                  Referrals
                </a>
                <a className="dropdown-item" href="setting.html">
                  <i className="la la-gear mr-2" />
                  Settings
                </a>
                <Link to="#" className="dropdown-item" onClick={() => console.log("first")}>
                  <i className="la la-power-off mr-2" />
                  Log out s4ira
                </Link>
              </div>
            </div>
          </div>
          {/* end tab-pane */}
        </div>
      </div>
    </header>
  );
}

export default Header;
