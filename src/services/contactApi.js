import axios from "axios";
import { getSession } from "../utils/SessionUtils";

const api_url = process.env.REACT_APP_API;

/* Setting the header for the axios request. */
const config = {
  headers: { Authorization: `Bearer ${getSession("token")}` },
};
export async function postMessage(objet, description, email, phone, file) {
  const frmData = new FormData();
  frmData.append("objet", objet);
  frmData.append("description", description);
  frmData.append("email", email);
  frmData.append("phone", phone);
  frmData.append("file", file);
  const httpHeaders = {
    "Content-Type": "multipart/form-data",
    Authorization: `Bearer ${getSession("token")}`,
  };

  const options = {
    headers: httpHeaders,
  };
  try {
    const response = await axios.post(api_url + "contact", frmData, options);
    return response;
  } catch (error) {
    console.error(error);
  }
}
