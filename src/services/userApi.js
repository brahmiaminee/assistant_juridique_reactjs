import axios from "axios";
import { getSession } from "../utils/SessionUtils";

const api_url = process.env.REACT_APP_API;

/* Setting the header for the axios request. */
const config = {
  headers: { Authorization: `Bearer ${getSession("token")}` },
};

/**
 * It sends a POST request to the server with the form data
 * @param nom - nom,
 * @param prenom - "test"
 * @param email - "test@test.com"
 * @param password - "123456"
 * @param role - "user"
 * @param file - the file to upload
 * @returns The response object.
 */
export async function inscription(firstName, lastName, email, password, phone) {
  // const body = { nom: nom, prenom: prenom, email: email, password: password, role: role, isactiv: "1" };
  const frmData = new FormData();
  frmData.append("firstName", firstName);
  frmData.append("lastName", lastName);
  frmData.append("email", email);
  frmData.append("password", password);
  frmData.append("phone", phone);

  const httpHeaders = {
    "Content-Type": "multipart/form-data",
  };

  const options = {
    headers: httpHeaders,
  };

  try {
    const response = await axios.post(api_url + "register", frmData, options);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * It takes an email and password, and sends a POST request to the server with the email and password as the body.
 *
 * If the request is successful, it returns the response. If the request is unsuccessful, it logs the error to the console.
 * @param email - email,
 * @param password - "password"
 * @returns The response object from the server.
 */
export async function login(email, password) {
  const body = { email: email, password: password };
  try {
    console.log(body);
    const response = await axios.post(api_url + "login", body);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * It gets a user by id.
 * @param id - The id of the user you want to get.
 * @returns The response object contains the data, headers, and status code.
 */
export async function getUserById(id) {
  try {
    const response = await axios.get(api_url + "users/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function updateUser(firstName, lastName, phone, userId) {
  const frmData = new FormData();
  frmData.append("firstName", firstName);
  frmData.append("lastName", lastName);
  frmData.append("phone", phone);

  const httpHeaders = {
    "Content-Type": "multipart/form-data",
    Authorization: `Bearer ${getSession("token")}`,
  };

  const options = {
    headers: httpHeaders,
  };

  try {
    const response = await axios.put(api_url + "user/" + userId, frmData, options);
    return response;
  } catch (error) {
    console.error(error);
  }
}
