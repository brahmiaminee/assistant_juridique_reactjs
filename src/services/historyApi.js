import axios from "axios";
import { getSession } from "../utils/SessionUtils";

const api_url_v1 = process.env.REACT_APP_API;

/* Setting the header for the axios request. */
const config = {
  headers: { Authorization: `Bearer ${getSession("token")}` },
};

export async function getHistoryByUserId(userID) {
  try {
    const response = await axios.get(api_url_v1 + "history/user/" + userID, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}
